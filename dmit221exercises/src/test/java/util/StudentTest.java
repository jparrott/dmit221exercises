package util;

import static org.junit.Assert.*;

import java.util.Set;

import javax.validation.*;

import org.junit.BeforeClass;
import org.junit.Test;

import ca.nait.dmit.repository.Student;

public class StudentTest {
	private static Validator validator;
	  
	  @BeforeClass
	  public static void setUpBeforeClass() throws Exception
	  {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	    validator = factory.getValidator();
	  }
	  
	  @Test
	  public void testNullFirstname()
	  {
		Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
			Student.class, "firstName", null);
		assertEquals(1, constraintViolations.size() );
		assertEquals("First name cannot be null", constraintViolations.iterator().next().getMessage() );
	  }

	  @Test
	  public void testMinLengthFirstname()
	  {
		Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
			Student.class, "firstName", "J");
		assertEquals(1, constraintViolations.size() );
		assertEquals("First name must contain between 2 to 25 characters", constraintViolations.iterator().next().getMessage() );
	  }  
	  
	  @Test
	  public void testMaxLengthFirstname()
	  {
		Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
			Student.class, "firstName", "12345678901234567890123456");
		assertEquals(1, constraintViolations.size() );
		assertEquals("First name must contain between 2 to 25 characters", constraintViolations.iterator().next().getMessage() );
	  }

		@Test
	  public void testValidFirstname()
	  {
		Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
			Student.class, "firstName", "YourFirstName");
		assertEquals(0, constraintViolations.size() );
	  }  

		 @Test
		  public void testLastNameEmpty()
		  {
			Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
				Student.class, "lastName", "");
			assertEquals(2, constraintViolations.size() );
			
		  }
		 
		 @Test
		  public void testLastNameMinChar()
		  {
			Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
				Student.class, "lastName", "P");
			assertEquals(1, constraintViolations.size() );
			assertEquals("Last name must contain at least 2 characters", constraintViolations.iterator().next().getMessage() );
		  }

		 @Test
		  public void testLastNameValid()
		  {
			Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
				Student.class, "lastName", "Parrott");
			assertEquals(0, constraintViolations.size() );
		  }
		 
		 @Test
		  public void testEmailAddressMissingAt()
		  {
			Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
				Student.class, "emailAddress", "usernamenait.ca");
			assertEquals(1, constraintViolations.size() );
			assertEquals("Email address is not in a recognizable format", constraintViolations.iterator().next().getMessage() );
		  }
		 
		 @Test
		  public void testMissingSecondLevelDomain()
		  {
			Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
				Student.class, "emailAddress", "usernamenait@.ca");
			assertEquals(1, constraintViolations.size() );
			assertEquals("Email address is not in a recognizable format", constraintViolations.iterator().next().getMessage() );
		  }
		 
		 @Test
		  public void testEmailAddressMissingTLD()
		  {
			Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
				Student.class, "emailAddress", "usernamenait@ca");
			assertEquals(1, constraintViolations.size() );
			assertEquals("Email address is not in a recognizable format", constraintViolations.iterator().next().getMessage() );
		  }
		 
		 @Test
		  public void testEmailAddressMissingTLD2Char()
		  {
			Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
				Student.class, "emailAddress", "username@nait.c");
			assertEquals(1, constraintViolations.size() );
			assertEquals("Email address is not in a recognizable format", constraintViolations.iterator().next().getMessage() );
		  }
		 
		 @Test
		  public void testEmailAddressValid()
		  {
			Set<ConstraintViolation<Student>> constraintViolations = validator.validateValue( 
				Student.class, "emailAddress", "username@nait.ca");
			assertEquals(0, constraintViolations.size() );
			
		  }
}
