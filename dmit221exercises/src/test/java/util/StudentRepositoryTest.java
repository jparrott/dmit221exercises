package util;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import ca.nait.dmit.repository.Student;
import ca.nait.dmit.repository.StudentRepositoryJdbcImpl;
import ca.nait.dmit.repository.StudentRepository;

public class StudentRepositoryTest {

private StudentRepository studentRespository = StudentRepositoryJdbcImpl.getInstance();
	
	@Before
	public void setUp() throws Exception {
		Connection dbConnection = DriverManager.getConnection("jdbc:postgresql://localhost:5432/dmit221db", "James", "iB00kG42");
		ScriptRunner runner = new ScriptRunner(dbConnection, false, true);
		runner.runScript(new BufferedReader( new FileReader("/home/mpoulin6/git/dmit221exercises/dmit221exercises/src/test/java/util/test-data.sql") ) );
	}
	
	
	@Test
	public void testCount() throws Exception {
		assertEquals( 7, studentRespository.count() ) ;
	}

	@Test
	public void testDeleteStudent() throws Exception {
		Student dmit172 = new Student();
		dmit172.setStudentId("DMIT172");
		studentRespository.delete(dmit172);
		assertFalse( studentRespository.exists("DMIT172") );
	}

	@Test
	public void testDeleteString() throws Exception {
		assertTrue( studentRespository.exists("111112222") );
		studentRespository.delete("111112222");
		assertFalse( studentRespository.exists("DMIT172") );
	}

	@Test
	public void testDeleteAll() throws Exception {
		studentRespository.deleteAll();
		assertEquals( 0, studentRespository.count() );
	}

	@Test
	public void testExists() throws Exception {
		assertFalse( studentRespository.exists("DMIT221") );
		assertTrue( studentRespository.exists("111112222") );
	}

	@Test
	public void testFindAll() throws Exception {
		assertEquals( 7, studentRespository.findAll().size() );
	}

//	@Test
//	public void testFindAllByName() throws Exception {
//		assertEquals( 2, studentRespository.findAllByName("Programming").size() );
//	}

	@Test
	public void testFindOne() throws Exception {
		assertNull( studentRespository.findOne("DMIT221") );
		assertNotNull( studentRespository.findOne("111112222") );
	}

	@Test
	public void testSave() throws Exception {
		Student dmit221 = new Student();
		dmit221.setStudentId("111112224");
		dmit221.setFirstName("James");
		dmit221.setLastName("Parrott");
		dmit221.setEmailAddress("derp@herp.com");
		dmit221.setPassword("derp");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date futureDate = sdf.parse("1960-12-25");
		dmit221.setBirthDate(futureDate);
		studentRespository.save(dmit221);
		assertTrue( studentRespository.exists( dmit221.getStudentId() ) );
		assertNotNull( studentRespository.findOne( dmit221.getStudentId() ) );
	}

}
