package ca.nait.dmit.repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


/**
 * JDBC implementation of the CourseRepository interface.
 *  
 * @author Sam Wu
 * @version 2012.10.24
 *
 */
public class StudentRepositoryJdbcImpl extends JdbcImpl implements StudentRepository {
	
	protected static final String TABLE_NAME = "student";
	protected static final String PRIMARY_KEY_COLUMN_NAME = "studentid";
	protected static final String ALL_COLUMN_NAMES = "studentid, firstname, lastname, email, password, birthdate";
	protected static final String UPDATE_COLUMNS = "firstname = ?, lastname = ?, email = ?, password = ?, birthdate = ?";
	
	// Implementation of Singleton design pattern
	private StudentRepositoryJdbcImpl() {
		super(StudentRepositoryJdbcImpl.class);
	}
	private static class SingletonHolder {
		private static final StudentRepository INSTANCE = new StudentRepositoryJdbcImpl();
	}
	public static StudentRepository getInstance() {
		return SingletonHolder.INSTANCE;
	}

	
	@Override
	public long count() throws SQLException  {
		long count = 0;

		dbConnection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		String sql = "SELECT count(" + PRIMARY_KEY_COLUMN_NAME + ") FROM " + TABLE_NAME;	
		dbStatement = dbConnection.prepareStatement(sql);
		ResultSet rs = dbStatement.executeQuery();
		if( rs.next() ) {
			count = rs.getLong(1);
		}
		
		try {
			dbConnection.close();
		} catch (SQLException e) {
			log.error( e.getMessage() );
		}
		
		return count;
	}

	@Override
	public void delete(Student student) throws Exception {
		delete( student.getStudentId() );
	}

	@Override
	public void delete(String courseId) throws Exception {
		dbConnection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		String sql = "DELETE FROM " + TABLE_NAME + " WHERE " + PRIMARY_KEY_COLUMN_NAME + " = ?";	
		dbStatement = dbConnection.prepareStatement(sql);
		dbStatement.setString(1, courseId);
		dbStatement.executeUpdate();
		try {
			dbConnection.close();
		} catch (SQLException e) {
			log.error( e.getMessage() );
		}
	}

	@Override
	public void deleteAll() throws Exception {
		dbConnection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		String sql = "DELETE FROM " + TABLE_NAME;	
		dbStatement = dbConnection.prepareStatement(sql);
		dbStatement.executeUpdate();

		try {
			dbConnection.close();
		} catch (SQLException e) {
			log.error( e.getMessage() );
		}
	}

	@Override
	public boolean exists(String courseid) throws Exception {
		boolean exists = false;

		dbConnection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		String sql = "SELECT " + PRIMARY_KEY_COLUMN_NAME + " FROM " + TABLE_NAME + " WHERE " + PRIMARY_KEY_COLUMN_NAME + " = ?";	
		dbStatement = dbConnection.prepareStatement(sql);
		dbStatement.setString(1, courseid);
		ResultSet rs = dbStatement.executeQuery();
		if( rs.next() ) {
			exists = true;
		}

		try {
			dbConnection.close();
		} catch (SQLException e) {
			log.error( e.getMessage() );
		}

		return exists;
	}

	@Override
	public List<Student> findAll() throws Exception {
		List<Student> students = new ArrayList<>();

		dbConnection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		String sql = "SELECT " + ALL_COLUMN_NAMES + " FROM " + TABLE_NAME;	
		dbStatement = dbConnection.prepareStatement(sql);
		ResultSet rs = dbStatement.executeQuery();
		while( rs.next() ) {
			students.add( createStudent(rs) );
		}
	
		try {
			dbConnection.close();
		} catch (SQLException e) {
			log.error( e.getMessage() );
		}

		return students;
	}

	@Override
	public List<Student> findAllByName(String partialCourseName) throws Exception {
		List<Student> courses = new ArrayList<>();

		dbConnection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		String sql = "SELECT " + ALL_COLUMN_NAMES + " FROM " + TABLE_NAME + " WHERE firstname LIKE ?";	
		dbStatement = dbConnection.prepareStatement(sql);
		dbStatement.setString(1, "%" + partialCourseName + "%");
		ResultSet rs = dbStatement.executeQuery();
		while( rs.next() ) {
			courses.add( createStudent(rs) );
		}

		try {
		dbConnection.close();
		} catch (SQLException e) {
			log.error( e.getMessage() );
		}

		return courses;
	}

	@Override
	public Student findOne(String courseId) throws Exception {
		Student course = null;

		dbConnection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		String sql = "SELECT " + ALL_COLUMN_NAMES + " FROM " + TABLE_NAME + " WHERE " + PRIMARY_KEY_COLUMN_NAME + " = ?";	
		dbStatement = dbConnection.prepareStatement(sql);
		dbStatement.setString(1, courseId);
		ResultSet rs = dbStatement.executeQuery();
		if( rs.next() ) {
			course = createStudent(rs);
		}

		try {
			dbConnection.close();
		} catch (SQLException e) {
			log.error( e.getMessage() );
		}
		
		return course;
	}
//studentid, firstname, lastname, email, password, birthdate
	@Override
	public void save(Student student)  throws Exception {

		if( exists(student.getStudentId()) ) {
			dbConnection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			String sql = "UPDATE " + TABLE_NAME + " SET " + UPDATE_COLUMNS + "WHERE " + PRIMARY_KEY_COLUMN_NAME + " = ?";	
			dbStatement = dbConnection.prepareStatement(sql);
			
			dbStatement.setString(1, student.getFirstName() );
			dbStatement.setString(2, student.getLastName() );	
			dbStatement.setString(3, student.getEmailAddress() );		
			dbStatement.setString(4, student.getPassword() );	
			dbStatement.setDate(5, new java.sql.Date(student.getBirthDate().getTime()) );
			dbStatement.setString(6, student.getStudentId() );
			
		} else  {
			dbConnection = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			String sql = "INSERT INTO " + TABLE_NAME + "(" + ALL_COLUMN_NAMES + ") VALUES(?,?,?,?,?,?)";	
			dbStatement = dbConnection.prepareStatement(sql);
			dbStatement.setString(1, student.getStudentId() );
			dbStatement.setString(2, student.getFirstName() );
			dbStatement.setString(3, student.getLastName() );	
			dbStatement.setString(4, student.getEmailAddress() );		
			dbStatement.setString(5, student.getPassword() );	
			dbStatement.setDate(6, new java.sql.Date(student.getBirthDate().getTime() ) );
										
		}
		
		dbStatement.executeUpdate();

		try {
			dbConnection.close();
		} catch (SQLException e) {
			log.error( e.getMessage() );
		}
	}
	
	protected Student createStudent(ResultSet rs) throws SQLException {
		Student student = new Student();
		student.setStudentId( rs.getString("studentId") );
		student.setFirstName( rs.getString("firstName") );
		student.setEmailAddress( rs.getString("email") );
		student.setLastName( rs.getString("lastName") );
		student.setPassword( rs.getString("password") );
		student.setBirthDate( rs.getDate("birthdate") );
		
		return student;
	}


	

}

