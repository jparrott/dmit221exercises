package ca.nait.dmit.repository;

import org.apache.log4j.Logger; 

import java.sql.*;	// import all JDBC version 1 classes 

/**
 * This base contains common fields required to access a database
 * using JDBC and is designed to be inherited from another class.
 * 
 * @author Sam Wu
 * @version 2009.03.09
 */
public class JdbcImpl {

	/** The Connection object for managing a connection to the database. */
	protected Connection dbConnection;
	/** The PreparedStatement object for executing SQL statements. */
	protected PreparedStatement dbStatement;
	/** The database connection string */
	protected String dbUrl = "jdbc:postgresql://localhost:5432/dmit221db";
	/** The database account username */
	protected String dbUserName = "James";
	/** The database account password */
	protected String dbPassword = "iB00kG42";
	/** The Logger for logging error messages */
	protected static Logger log = Logger.getLogger(
				JdbcImpl.class);			
	
	/** Initialize the PostgreSQL JDBC driver */
	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch( ClassNotFoundException e ) {
			log.error("Error loading JDBC driver");
		}
	}
	
	protected JdbcImpl(@SuppressWarnings("rawtypes") Class classToLog) {
		log = Logger.getLogger( classToLog );
	}

	public void init(String dbUrl, String dbUserName, String dbPassword) {
		this.dbUrl = dbUrl;
		this.dbUserName = dbUserName;
		this.dbPassword = dbPassword;
	}

}


