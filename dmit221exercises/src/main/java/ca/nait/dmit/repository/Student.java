package ca.nait.dmit.repository;

import java.util.Date;

import javax.validation.constraints.*;

import org.hibernate.validator.constraints.*;


public class Student {
	@NotNull(message="First name cannot be null")
	@Size(min=2,max=25,message="First name must contain between 2 to 25 characters")
	 private String firstName;
	@NotEmpty(message = "Last name cannot be empty")
	@Size(min = 2, message = "Last name must contain at least 2 characters")
	 private String lastName;
	//@Email(message="Email address is not in a recognizable format")
	@Pattern(regexp="^[_a-z0-9-]+(\\.[_a-z0-9-]+)*@[a-z0-9-]+(\\.[a-z0-9-]+)*(\\.[a-z]{2,4})$",
			message="Email address is not in a recognizable format")
	 private String emailAddress;
	@Pattern(regexp = "^.*(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$", 
			message = "Password must be at least 8 characters and must contain at least one lower case letter, one upper case letter and one digit")
	 private String password;
	
	@Pattern(regexp = "^[6-9]\\d{6}|2\\d{8}$", message = "StudentId is not in a recognizable format")
	 private String studentId;
	@Past(message="Birthdate must be a date in the past")
	 private Date birthDate;
	 
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	
	public Student() {
		super();
		
	}
	
	
}
