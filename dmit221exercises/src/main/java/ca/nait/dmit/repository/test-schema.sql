 DROP TABLE student;

CREATE TABLE student
(
  studentid character varying(10),
  firstname character varying(30),
  email character varying(50),
  lastname character varying(30),
  password character varying(20),
  birthdate character varying(20)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE student
  OWNER TO "James";
