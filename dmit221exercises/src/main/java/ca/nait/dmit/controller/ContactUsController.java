package ca.nait.dmit.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

import util.Gmail;
import util.JSFUtil;

@ManagedBean(name = "contactUsBean")
@RequestScoped
public class ContactUsController 
{
	private String name;
	private String email;
	private String message;
	
	public String getName() 
	{
		return name;
	}
	public void setName(String name) 
	{
		this.name = name;
	}
	public String getEmail() 
	{
		return email;
	}
	public void setEmail(String email) 
	{
		this.email = email;
	}
	public String getMessage() 
	{
		return message;
	}
	public void setMessage(String message) 
	{
		this.message = message;
	}
//	public String sendMessage() //action not actionlistener
//	{
//		String nextPage = null; //can put url here if want to go to another page
//		Gmail mailer = new Gmail("", "");
//		try 
//		{
//			mailer.sendmail("mmichpp@gmail.com", email, "from contact page", message);
//			JSFUtil.addInfoMessage("Email sent successfully. Thank you for contacting us.");
//			mailer.sendmail(email, "mmichpp@gmail.com", "Auto Response from Website", "Thank you for contacting us. We will respond within 48 hours.");
//		}
//		catch (Exception e) 
//		{
//			// TODO Auto-generated catch block
//			JSFUtil.addErrorMessage("Could not send. Please try again soon.");
//			//e.printStackTrace();
//		}
//		
//		return nextPage;
//	}
	public void sendMessage() //actionlistener 
	{	
		String gmailUsername = JSFUtil.getInitParameter("gmail.username");
		String gmailPassword = JSFUtil.getInitParameter("gmail.password");
		
		Gmail sendmail = new Gmail(gmailUsername, gmailPassword);
		try {
			sendmail.sendmail(
					gmailUsername, 
					email, 
					"Message from " + name, 
					message);
			JSFUtil.addInfoMessage("Successfully sent message.");
			// add the code to send an email to the user that
			// the message has been received and they should
			// expect a response within 48 hours.

			// clear the text in the web form
			name = email = message = "";
			
		} catch (Exception e) {
			JSFUtil.addErrorMessage("Server error. Please try again later.");
		}	
	}	
	
}
