package ca.nait.dmit.controller;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.ViewScoped;


import util.JSFUtil;
import util.Loan;
import util.LoanSchedule;

@ManagedBean(name = "loanBean")
@ViewScoped
public class LoanController {
	private Loan loan = new Loan();
	
	LoanSchedule[] loanScheduleTable;
	
	public LoanSchedule[] getLoanScheduleTable() {
		return loanScheduleTable;
	}

	public void setLoanScheduleTable(LoanSchedule[] loanScheduleTable) {
		this.loanScheduleTable = loanScheduleTable;
	}

	public Loan getloan()
	{
		return loan;		
	}
	
	public void setLoan(Loan loan)
	{
		this.loan = loan;
	}
	
	public void calculatePayment()
	{
		JSFUtil.addInfoMessage(
				"Your Monthly Payment is: "
				+loan.getMonthlyPayment()
				);
		loanScheduleTable = loan.getLoanScheduleArray();
	}
	
//	public int getIntPaid()
//	{
//		int intPaid = 0;
//		
//		
//		return intPaid;
//	}
}
