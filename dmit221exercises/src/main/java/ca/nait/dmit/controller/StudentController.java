package ca.nait.dmit.controller;



import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import util.JSFUtil;

import ca.nait.dmit.repository.Student;
import ca.nait.dmit.repository.StudentRepositoryJdbcImpl;
import ca.nait.dmit.repository.StudentRepository;

@ManagedBean(name="studentBean")
@SessionScoped
public class StudentController {

	private StudentRepository studentRepository = StudentRepositoryJdbcImpl.getInstance();
	private Student currentStudent = new Student();
	
	
	
	public Student getCurrentStudent() {
		return currentStudent;
	}



	public void setCurrentStudent(Student currentStudent) {
		this.currentStudent = currentStudent;
	}



	public void saveStudent() {
		try {
			studentRepository.save(currentStudent);
			currentStudent = new Student();
			JSFUtil.addInfoMessage("Successfully saved student information.");
		} catch (Exception e) {
			JSFUtil.addErrorMessage("Error! The following exception has occurred: " + e.getMessage() );
		}
	}
}

